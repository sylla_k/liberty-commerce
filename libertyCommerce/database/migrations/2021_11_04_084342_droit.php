<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Droit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('droit', function (Blueprint $table) {
            $table->id()->unique();
            $table->Boolean('lire');
            $table->Boolean('ecrire');
            $table->boolean('suprimer');
            $table->boolean('modifier');
            $table->integer('id_role');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
