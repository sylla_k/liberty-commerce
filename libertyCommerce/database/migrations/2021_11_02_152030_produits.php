<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Produits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id()->unique();
            $table->string('libelle')->unique();
            $table->string('description');
            $table->integer('prix');
            $table->integer('stock');
            $table->string('image');
            $table->string('categorie');
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}