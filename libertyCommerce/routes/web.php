<?php
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PanierController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/admin', [ProduitController::class, 'store']);
Route::get('/admin', [ProduitController::class, 'create']);
Route::get('/admin', [ProduitController::class, 'index']);
Route::get('/admin2', [ProduitController::class, 'show']);
Route::post('/admin2', [ProduitController::class, 'update']);
Route::get('/register', [UsersController::class, 'create']);
Route::post('/register', [UsersController::class,  'store']);
Route::get('/panier', [PanierController::class, 'create']);
