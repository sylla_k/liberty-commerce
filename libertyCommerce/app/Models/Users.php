<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'name',
        'firstname',
        'adress',
        'email',
        'password',
        'date_inscription',
        'id_role',
    ];
}
