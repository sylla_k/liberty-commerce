<?php

namespace App\Http\Controllers;
use App\Models\Produits;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    public function create()
    {
        return view('admin');
    }
    public function create1()
    {
        return view('admin2');
    }
  

    public function store(Request $request)
    {
       
        $produit = new Produits;
       
        $produit->id = $request->id;
        $produit->libelle = $request->libelle;
        $produit->description = $request->description;
        $produit->prix = $request->prix;
        $produit->stock = $request->stock;
        $produit->image = $request->image;
        $produit->categorie = $request->categorie;

        $produit->save();
     
        return view('admin');
    }

    public function index()
    {
       
        $produitss = Produits::all();
       return view ('admin',[
           'produitss'=>$produitss
       ]);
     
    }
    public function show($id)
    {
       
      $produits= Produits::find($id);

      return view('admin2',[
          'produits'=>$produits
      ]);
     
    }
    public function update(Request $request)
    {
       
       
        return view('admin');
    }


  

    
}
