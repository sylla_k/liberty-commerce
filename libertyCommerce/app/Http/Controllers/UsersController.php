<?php

namespace App\Http\Controllers;
use App\Models\Users;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function create()
    {
        return view('register');
    }
    public function store(Request $request)
    {
       
        $users = new Users;
       
        $users->name = $request->name;
        $users->firstname = $request->firstname;
        $users->adress = $request->adress;
        $users->email = $request->email;
        $users->password = $request->password;
        $users->id_role = 2;



        $users->save();
     
        return view('register');
    }
    public function index()
    {
       
        $users = Users::all();
     
        return view('admin',['users' => $users ]);
    }
    
}
