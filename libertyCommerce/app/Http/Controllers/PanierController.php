<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PanierController extends Controller
{
    public function create()
    {
        return view('panier');
    }
    /**
     * 
     * 
     * 
     * 
     */
    public function store(Request $request)
    {
       
        $panier = new Panier;
       
        
        $panier->quantite = $request->quantite;
        $panier->prix_total = $request->prix_total;
        $panier->id_produits = $request->id_produits;
        $panier->id_users = $request->id_users;
    

        $panier->save();
     
        return view('index');
    }
    public function index()
    {
       
        $panier = Panier::all();
     
        return view('panier',['panier' => $panier ]);
    }
    

  
}
